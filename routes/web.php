<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [App\Http\Controllers\CrudMahasiswaController::class, 'halamanutama']);
Route::get('/tambah', [App\Http\Controllers\CrudMahasiswaController::class, 'halamantambah']);
Route::post('/insert', [App\Http\Controllers\CrudMahasiswaController::class, 'insert']);
Route::get('/edit/{id}', [App\Http\Controllers\CrudMahasiswaController::class, 'halamanubah']);
Route::post('/update', [App\Http\Controllers\CrudMahasiswaController::class, 'update']);
Route::get('/hapus/{id}', [App\Http\Controllers\CrudMahasiswaController::class, 'hapus']);